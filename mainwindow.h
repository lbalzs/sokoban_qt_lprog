#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QKeyEvent>
#include "game.h"
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QPushButton>
#include <QLabel>
#include <QSound>
#include "sounds.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void loadMap(int id);
    void keyPressEvent(QKeyEvent * event);
    void updateSideBar();
    ~MainWindow();

private:
    Game *game;
    QGraphicsScene *scene;
    QGraphicsView *view;
    SokobanMap *currentMap;
    int currentMapId;
    QPushButton *nextButton;
    QPushButton *previousButton;
    QPushButton *resetButton;
    QPushButton *musicButton;
    QPushButton *soundButton;
    QPushButton *sudoButton;
    QLabel *mapID;
    QLabel *totalLevel;
    QLabel *moves;
    QLabel *tries;
    QLabel *solved;
    QLabel *solvedTotal;
    QLabel *totalTries;
    QLabel *totalMoves;

private slots:
    void nextLevelButtonHandler();
    void previousLevelButtonHandler();
    void resetButtonHandler();
    void musicHandler();
    void soundHandler();
    void sudoButtonHandler();
};

#endif // MAINWINDOW_H
