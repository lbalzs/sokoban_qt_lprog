#-------------------------------------------------
#
# Project created by QtCreator 2018-05-11T16:17:03
#
#-------------------------------------------------

QT       += core gui
QT       += multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = LProgSokoban
TEMPLATE = app


SOURCES += main.cpp \
    game.cpp \
    sokobanmap.cpp \
    mainwindow.cpp \
    sounds.cpp

HEADERS  += \
    game.h \
    sokobanmap.h \
    mainwindow.h \
    sounds.h

FORMS    +=

RESOURCES += \
    res.qrc

DISTFILES +=
