#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    this->setFixedSize(1280,720);
    game = new Game();
    game->loadMapsFromFile();
    scene = new QGraphicsScene();

    this->setWindowTitle("LinuxProgramming - QSokoban - LadanyiBalazs@2018BME");

    currentMapId = 0;
    loadMap(currentMapId);

    nextButton = new QPushButton("Next Level", this);
    nextButton->setGeometry(QRect(QPoint(1040,570), QSize(210,50)));

    previousButton = new QPushButton("Previous Level", this);
    previousButton->setGeometry(QRect(QPoint(1040,500), QSize(210,50)));

    resetButton = new QPushButton("Reset Level", this);
    resetButton->setGeometry(QRect(QPoint(1040,430), QSize(210,50)));

    musicButton = new QPushButton(this);
    musicButton->setGeometry(QRect(QPoint(1040,640), QSize(50,50)));
    musicButton->setIcon(QIcon(":/images/images/music.png"));
    musicButton->setIconSize(QSize(50,50));

    soundButton = new QPushButton(this);
    soundButton->setGeometry(QRect(QPoint(1123,640), QSize(50,50)));
    soundButton->setIcon(QIcon(":/images/images/sound.png"));
    soundButton->setIconSize(QSize(50,50));

    sudoButton = new QPushButton(this);
    sudoButton->setGeometry(QRect(QPoint(1203,640), QSize(50,50)));
    sudoButton->setIcon(QIcon(":/images/images/sudoOff.png"));
    sudoButton->setIconSize(QSize(50,50));

    mapID = new QLabel("Level " + QString::number(currentMapId+1), this);
    mapID->setStyleSheet("font-size:35px; color:#FFCBAA;font-weight:bold;");
    mapID->setGeometry(QRect(QPoint(1020,15), QSize(250,40)));
    mapID->setAlignment(Qt::AlignCenter);

    totalLevel = new QLabel("out of " + QString::number(game->getNumberOfMaps()),this);
    totalLevel->setStyleSheet("font-size:16px; color:#FFCBAA;");
    totalLevel->setGeometry(QRect(QPoint(1020,45), QSize(250,30)));
    totalLevel->setAlignment(Qt::AlignCenter);

    moves = new QLabel("Moves: " + QString::number(currentMap->getMoves()),this);
    moves->setStyleSheet("font-size:22px; color:#FFCBAA;");
    moves->setGeometry(QRect(QPoint(1020,100), QSize(250,30)));
    moves->setAlignment(Qt::AlignCenter);

    tries = new QLabel("Tries: " + QString::number(currentMap->getTries()),this);
    tries->setStyleSheet("font-size:22px; color:#FFCBAA;");
    tries->setGeometry(QRect(QPoint(1020,140), QSize(250,30)));
    tries->setAlignment(Qt::AlignCenter);

    solved = new QLabel("Solved: "+ QString::number(currentMap->getSolves()), this);
    solved->setStyleSheet("font-size:22px; color:#FFCBAA;");
    solved->setGeometry(QRect(QPoint(1020,180), QSize(250,30)));
    solved->setAlignment(Qt::AlignCenter);

    solvedTotal = new QLabel("Maps solved: " + QString::number(game->getNumberOfMapsSolved()) + "/" + QString::number(game->getNumberOfMaps()), this);
    solvedTotal->setStyleSheet("font-size:22px; color:#FFCBAA;");
    solvedTotal->setGeometry(QRect(QPoint(1020,260), QSize(250,30)));
    solvedTotal->setAlignment(Qt::AlignCenter);

    totalTries = new QLabel("Total tries: "+ QString::number(game->getNumberOfTotalTries()), this);
    totalTries->setStyleSheet("font-size:22px; color:#FFCBAA;");
    totalTries->setGeometry(QRect(QPoint(1020,300), QSize(250,30)));
    totalTries->setAlignment(Qt::AlignCenter);

    totalMoves = new QLabel("Total moves: "+ QString::number(game->getNumberOfTotalMoves()), this);
    totalMoves->setStyleSheet("font-size:22px; color:#FFCBAA;");
    totalMoves->setGeometry(QRect(QPoint(1020,340), QSize(250,30)));
    totalMoves->setAlignment(Qt::AlignCenter);

    connect(nextButton, SIGNAL(released()), this, SLOT(nextLevelButtonHandler()));
    connect(previousButton, SIGNAL(released()), this, SLOT(previousLevelButtonHandler()));
    connect(resetButton, SIGNAL(released()), this, SLOT(resetButtonHandler()));
    connect(musicButton, SIGNAL(released()), this, SLOT(musicHandler()));
    connect(soundButton, SIGNAL(released()), this, SLOT(soundHandler()));
    connect(sudoButton, SIGNAL(released()), this, SLOT(sudoButtonHandler()));

    updateSideBar();

}

void MainWindow::loadMap(int id)
{
    currentMap = game->getMap(id);
    currentMap->drawMap(scene);
    view = new QGraphicsView(scene);
    view->setStyleSheet("background-color:#803F15");
    this->setCentralWidget(view);
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    currentMap->keyPressed(event);
    if(currentMap->isWin()){
        qDebug() << "Level" << currentMapId+1 << "solved!";
        currentMap = game->getMap(currentMapId+1);
        currentMapId++;
    }
    updateSideBar();
    scene->clear();
    currentMap->drawMap(scene);
}

void MainWindow::updateSideBar()
{
    mapID->setText("Level " + QString::number(currentMapId+1));
    moves->setText("Moves: " + QString::number(currentMap->getMoves()));
    tries->setText("Tries: " + QString::number(currentMap->getTries()));
    solved->setText("Solved: "+ QString::number(currentMap->getSolves()));
    solvedTotal->setText("Maps solved: " + QString::number(game->getNumberOfMapsSolved()) + "/" + QString::number(game->getNumberOfMaps()));
    totalTries->setText("Total tries: "+ QString::number(game->getNumberOfTotalTries()));
    totalMoves->setText("Total moves: "+ QString::number(game->getNumberOfTotalMoves()));

    previousButton->setEnabled(true);
    nextButton->setEnabled(true);

    if(currentMapId == 0){
        previousButton->setDisabled(true);
    }
    if(currentMapId == game->getNumberOfMaps()-1){
        nextButton->setDisabled(true);
    }
    if(!game->getMap(currentMapId)->isSolved() && !game->isSudo()){
        nextButton->setDisabled(true);
    }
}

void MainWindow::nextLevelButtonHandler()
{
    if(currentMapId+1 > game->getNumberOfMaps()-1){
        qDebug() << "No more maps!";
    }
    else{
        currentMap = game->getMap(currentMapId+1);
        currentMapId++;
        scene->clear();
        currentMap->drawMap(scene);
        updateSideBar();
        qDebug() << "Next map loaded!";
    }
}

void MainWindow::previousLevelButtonHandler()
{
    if(currentMapId-1 < 0){
        qDebug() << "This is the first map!";
    }
    else{
        currentMap = game->getMap(currentMapId-1);
        currentMapId--;
        scene->clear();
        currentMap->drawMap(scene);
        updateSideBar();
        qDebug() << "Previous map loaded!";

    }
}

void MainWindow::resetButtonHandler()
{
    currentMap->resetMap();
    scene->clear();
    currentMap->drawMap(scene);
    updateSideBar();
    qDebug() << "Map reset!";
}

void MainWindow::musicHandler()
{
    if(game->getMusicPlayer()->isPlaying()){
        game->getMusicPlayer()->disableMusic();
        musicButton->setIcon(QIcon(":/images/images/musicOff.png"));
        qDebug() << "Music disabled!";
    }
    else{
        game->getMusicPlayer()->enableMusic();
        musicButton->setIcon(QIcon(":/images/images/music.png"));
        qDebug() << "Music enabled!";
    }
}

void MainWindow::soundHandler()
{
    if(game->getMusicPlayer()->isSoundsEnabled()){
        game->getMusicPlayer()->disableSounds();
        soundButton->setIcon(QIcon(":/images/images/soundOff.png"));
        qDebug() << "Sound effects disabled!";
    }
    else{
        game->getMusicPlayer()->enableSounds();
        soundButton->setIcon(QIcon(":/images/images/sound.png"));
        qDebug() << "Sound effects enabled!";
    }
}

void MainWindow::sudoButtonHandler()
{
    if(game->isSudo()){
        game->setSudo(false);
        sudoButton->setIcon(QIcon(":/images/images/sudoOff.png"));
        qDebug() << "Sudo disabled!";
    }
    else{
        game->setSudo(true);
        sudoButton->setIcon(QIcon(":/images/images/sudo.png"));
        qDebug() << "Sudo enabled!";
    }
    updateSideBar();
}

MainWindow::~MainWindow()
{
    delete scene;
    delete view;
    delete game;
    delete nextButton;
    delete previousButton;
    delete resetButton;
    delete musicButton;
    delete soundButton;
    delete sudoButton;
    delete mapID;
    delete totalLevel;
    delete moves;
    delete tries;
    delete solved;
    delete solvedTotal;
    delete totalTries;
    delete totalMoves;
}
