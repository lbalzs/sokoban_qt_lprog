#include "sounds.h"

Sounds::Sounds()
{
    isSoundsEnabledVar = true;
}

void Sounds::enableMusic()
{
    backgroundMusic = new QSound(":/audio/sounds/music.wav");
    backgroundMusic->setLoops(500);
    backgroundMusic->play();
}

void Sounds::disableMusic()
{
    backgroundMusic->stop();
}

void Sounds::enableSounds()
{
    isSoundsEnabledVar = true;
}

void Sounds::disableSounds()
{
    isSoundsEnabledVar = false;
}

void Sounds::playMoveSound()
{
    if(isSoundsEnabledVar){
        QSound::play(":/audio/sounds/moveChar.wav");
    }
}

void Sounds::playErrorSound()
{
    if(isSoundsEnabledVar){
        QSound::play(":/audio/sounds/error.wav");
    }
}

void Sounds::playBoxSound()
{
    if(isSoundsEnabledVar){
        QSound::play(":/audio/sounds/box.wav");
    }
}

void Sounds::playWinSound()
{
    if(isSoundsEnabledVar){
        QSound::play(":/audio/sounds/win.wav");
    }
}

bool Sounds::isPlaying()
{
    return !(backgroundMusic->isFinished());
}

bool Sounds::isSoundsEnabled()
{
    return isSoundsEnabledVar;
}

Sounds::~Sounds()
{
    delete backgroundMusic;
}
