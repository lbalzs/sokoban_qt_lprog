#ifndef GAME_H
#define GAME_H

#include "sokobanmap.h"
#include <QList>
#include <QWidget>
#include "sounds.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>

class Game
{
public:
    Game();
    void loadMapsFromFile();
    SokobanMap *getMap(int nr);
    int getNumberOfMaps();
    Sounds *getMusicPlayer();
    int getNumberOfMapsSolved();
    int getNumberOfTotalTries();
    int getNumberOfTotalMoves();
    void setSudo(bool sudo);
    bool isSudo();
    ~Game();


private:
    QList<SokobanMap*> *myMaps;
    Sounds *musicPlayer;
    bool sudo;
};

#endif // GAME_H
