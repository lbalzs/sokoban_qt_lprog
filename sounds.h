#ifndef SOUNDS_H
#define SOUNDS_H

#include <QSound>

class Sounds
{
public:
    Sounds();
    void enableMusic();
    void disableMusic();
    void enableSounds();
    void disableSounds();
    void playMoveSound();
    void playErrorSound();
    void playBoxSound();
    void playWinSound();
    bool isPlaying();
    bool isSoundsEnabled();
    ~Sounds();

private:
    bool isSoundsEnabledVar;
    QSound *backgroundMusic;
};

#endif // SOUNDS_H
