# Sokoban QT for LinuxProgramming

Sokoban game written in QT for University lecture

#### Build
```bash
qmake LProgSokoban.pro
make
```

#### Run
```bash
./LProgSokoban
```
#### Class Diagram
![Class Diagram](https://gitlab.com/lbalzs/sokoban_qt_lprog/raw/master/documentation/pic/QSokobanClassDiagram.png)

#### Game
![Screenshot](https://gitlab.com/lbalzs/sokoban_qt_lprog/raw/master/documentation/pic/GameScreenshot.png)
