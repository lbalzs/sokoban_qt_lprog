#ifndef SOKOBANMAP_H
#define SOKOBANMAP_H


#include <QString>
#include <QGraphicsScene>
#include <QDebug>
#include <QGraphicsPixmapItem>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QSound>
#include "sounds.h"


class SokobanMap
{
public:
    SokobanMap(QString [][21], Sounds *musicPlayer);
    QString at(int x, int y);
    void drawMap(QGraphicsScene *scene);
    void resetMap();
    void keyPressed(QKeyEvent *event);
    bool isWin();
    int getMoves();
    int getTotalMoves();
    int getTries();
    int getSolves();
    bool isSolved();
    ~SokobanMap();

private:
    QString originalStructure[14][21];
    QString inGameStructure[14][21];
    Sounds *musicPlayer;
    int originalCharacterPositionX;
    int originalCharacterPositionY;
    int characterPositionX;
    int characterPositionY;
    int movesOnMap;
    int totalMovesOnMap;
    int triesOnMap;
    int solved;
};

#endif // SOKOBANMAP_H
