#include "sokobanmap.h"

SokobanMap::SokobanMap(QString _structure[][21], Sounds *_musicPlayer)
{
    movesOnMap = 0;
    triesOnMap = 0;
    totalMovesOnMap = 0;
    solved = 0;
    musicPlayer = _musicPlayer;
    for(int i = 0; i < 14; i++){
        for(int j = 0; j < 20; j++){
            if(_structure[i][j] == "G"){
                characterPositionX = j;
                characterPositionY = i;
                originalCharacterPositionX = j;
                originalCharacterPositionY = i;
            }
            originalStructure[i][j] = _structure[i][j];
            inGameStructure[i][j] = _structure[i][j];
        }
    }
}

QString SokobanMap::at(int x, int y){
    return inGameStructure[x][y];
}

void SokobanMap::drawMap(QGraphicsScene *scene){
    QGraphicsPixmapItem *item;
    item = new QGraphicsPixmapItem(QPixmap(":/images/images/sideBar.png"));
    item->setPos(1020,10);
    scene->addItem(item);

    int x = 10;
    int y = 10;
    for(int i = 0; i < 14 ; i++){
        x=10;
        for(int j =0; j < 20; j++){
            if(at(i,j) == "W"){
                item = new QGraphicsPixmapItem(QPixmap(":/images/images/wall.png"));
            }
            if(at(i,j) == "E"){
                item = new QGraphicsPixmapItem(QPixmap(":/images/images/empty.png"));
            }
            if(at(i,j) == "T"){
                item = new QGraphicsPixmapItem(QPixmap(":/images/images/target.png"));
            }
            if(at(i,j) == "G"){
                item = new QGraphicsPixmapItem(QPixmap(":/images/images/character.png"));
            }
            if(at(i,j) == "B"){
                item = new QGraphicsPixmapItem(QPixmap(":/images/images/box.png"));
            }
            if(at(i,j) == "F"){
                item = new QGraphicsPixmapItem(QPixmap(":/images/images/box_t.png"));
            }
            item->setPos(x,y);
            scene->addItem(item);
            x += 50;
        }
        y += 50;
    }
}

void SokobanMap::resetMap(){
    for(int i = 0; i < 14; i++){
        for(int j = 0; j < 20; j++){
            inGameStructure[i][j] = originalStructure[i][j];
        }
    }
    characterPositionX = originalCharacterPositionX;
    characterPositionY = originalCharacterPositionY;
    movesOnMap = 0;
    triesOnMap++;
}

void SokobanMap::keyPressed(QKeyEvent *event)
{
    int x1,x2,y1,y2;

    switch(event->key()){
        case 87: // W
            x1 = 0;
            x2 = 0;
            y1 = -1;
            y2 = -2;

            qDebug() << "W pressed -> moving character UP";
            break;
        case 65: // A
            x1 = -1;
            x2 = -2;
            y1 = 0;
            y2 = 0;
            qDebug() << "A pressed -> moving character LEFT";
            break;
        case 83: // S
            x1 = 0;
            x2 = 0;
            y1 = 1;
            y2 = 2;
            qDebug() << "S pressed -> moving character DOWN";
            break;
        case 68: // D
            x1 = 1;
            x2 = 2;
            y1 = 0;
            y2 = 0;
            qDebug() << "D pressed -> moving character RIGHT";
            break;
        default:
            return;
            break;
    }
    if(inGameStructure[characterPositionY+y1][characterPositionX+x1] == "W"){
        qDebug() << "Moving is blocked by Wall";
        musicPlayer->playErrorSound();
    }
    if(inGameStructure[characterPositionY+y1][characterPositionX+x1] == "B" || inGameStructure[characterPositionY+y1][characterPositionX+x1] == "F"){
        if(inGameStructure[characterPositionY+y2][characterPositionX+x2] != "B"
                    && inGameStructure[characterPositionY+y2][characterPositionX+x2] != "W"
                    && inGameStructure[characterPositionY+y2][characterPositionX+x2] != "F"){
            musicPlayer->playBoxSound();
            movesOnMap++;
            totalMovesOnMap++;
            if(originalStructure[characterPositionY+y2][characterPositionX+x2] == "T"){
                inGameStructure[characterPositionY+y2][characterPositionX+x2] = "F";
            }
            else {
                inGameStructure[characterPositionY+y2][characterPositionX+x2] = "B";
            }
            inGameStructure[characterPositionY+y1][characterPositionX+x1] = "G";
            if(originalStructure[characterPositionY][characterPositionX] == "T"){
                inGameStructure[characterPositionY][characterPositionX] = "T";
            }
            else{
                inGameStructure[characterPositionY][characterPositionX] = "E";
            }
            characterPositionX += x1;
            characterPositionY += y1;
        }
        else{
            qDebug() << "Moving is blocked by Box";
            musicPlayer->playErrorSound();
        }
    }
    if(inGameStructure[characterPositionY+y1][characterPositionX+x1] == "E" || inGameStructure[characterPositionY+y1][characterPositionX+x1] == "T"){
        musicPlayer->playMoveSound();
        movesOnMap++;
        totalMovesOnMap++;
        inGameStructure[characterPositionY+y1][characterPositionX+x1] = "G";
        if(originalStructure[characterPositionY][characterPositionX] == "T"){
            inGameStructure[characterPositionY][characterPositionX] = "T";
        }
        else{
            inGameStructure[characterPositionY][characterPositionX] = "E";
        }
        characterPositionX += x1;
        characterPositionY += y1;
    }
}

bool SokobanMap::isWin()
{
    for(int i = 0; i < 14; i++){
        for(int j = 0; j < 20; j++){
            if(originalStructure[i][j] == "T" && inGameStructure[i][j] != "F") return false;
        }
    }
    resetMap();
    triesOnMap--;
    musicPlayer->playWinSound();
    solved++;
    triesOnMap++;
    return true;
}

int SokobanMap::getMoves()
{
    return movesOnMap;
}

int SokobanMap::getTotalMoves()
{
    return totalMovesOnMap;
}

int SokobanMap::getTries()
{
    return triesOnMap;
}

int SokobanMap::getSolves()
{
    return solved;
}

bool SokobanMap::isSolved()
{
    if(solved > 0) return true;
    return false;
}

SokobanMap::~SokobanMap()
{

}





