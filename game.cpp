#include "game.h"


Game::Game()
{
    musicPlayer = new Sounds();
    musicPlayer->enableMusic();
    musicPlayer->enableSounds();
    myMaps = new QList<SokobanMap*>();
    sudo = false;
}

void Game::loadMapsFromFile(){
    const char* path = ":/db/data/db.txt";
    qDebug() << "Loading maps from file " << path;
    QFile file(path);

    if(!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Cant open the database!";
    }

    QTextStream input(&file);

    QString tempArrayMap [14][21];
    int lineCounter = 0;
    int mapCounter = 0;
    QString line;

    while(!input.atEnd()) {
        lineCounter = 0;
        while(1){
            line = input.readLine();
            if(line == "") break;
            for(int i = 0; i < 20; i++){
                tempArrayMap[lineCounter][i] = QString(line.at(i));
            }
            lineCounter +=1;
        }
        SokobanMap *tempMap = new SokobanMap(tempArrayMap, musicPlayer);
        myMaps->append(tempMap);
        qDebug() << mapCounter << "map read!";
        mapCounter++;
    }

    file.close();
}
SokobanMap* Game::getMap(int nr){
    return myMaps->at(nr);
}

int Game::getNumberOfMaps()
{
    return myMaps->size();
}

Sounds *Game::getMusicPlayer()
{
    return musicPlayer;
}

int Game::getNumberOfMapsSolved()
{
    int countSolvedMaps = 0;
    for(int i = 0; i < myMaps->size(); i++){
        if(myMaps->at(i)->isSolved()) countSolvedMaps++;
    }
    return countSolvedMaps;
}

int Game::getNumberOfTotalTries()
{
    int countTotalTries = 0;
    for(int i = 0; i < myMaps->size(); i++){
        countTotalTries += myMaps->at(i)->getTries();
    }
    return countTotalTries;
}

int Game::getNumberOfTotalMoves()
{
    int countTotalMoves = 0;
    for(int i = 0; i < myMaps->size(); i++){
        countTotalMoves += myMaps->at(i)->getTotalMoves();
    }
    return countTotalMoves;
}

void Game::setSudo(bool sudo)
{
    this->sudo = sudo;
}

bool Game::isSudo()
{
    return sudo;
}

Game::~Game()
{
    for(int i = 0; i < myMaps->length(); i++){
        delete myMaps->at(i);
    }
    delete myMaps;
    delete musicPlayer;
}
